package br.com.proway.capgemini.controller;

import java.util.ArrayList;
import br.com.proway.capgemini.model.Contato;

public class ContatoController {

	/**
	 * Instância do objeto contato
	 */
	public Contato contato;
	public ArrayList<Contato>contatos = new ArrayList<Contato>();
	

		/**
		 * Adiciona contatos dentro do arraylist ( nome e chave pix)
		 *
		 *
		 * método recebe como paramatro objeto contato
		 * método retorna um arraylist
		 *
		 * @param contato
		 * @return contatos
		 */
		public ArrayList<Contato> salvarContato(Contato contato) {
			contatos.add(contato);
			return contatos;
		}

		/**
		 * Lista contatos dentro do arraylist (nome e a chave pix)
		 *
		 *
		 * método recebe como parametro objeto contato
		 * método retorna um arraylist
		 *
		 * @return contatos
		 */
		public ArrayList<Contato> listarContato(){
			return contatos;
		}

		/**
		 * Atualiza contatos dentro do arraylist (nome e chave pix)
		 *
		 *
		 * método recebe como parametro objeto contato
		 * método retorna um arraylist
		 *
		 * @param contato
		 * @return contatos
		 */
		public ArrayList<Contato> atualizarContato(Contato contato, int id){
			if(contatos.get(id) != null) {
				contatos.set(id,contato);
			}
			return contatos;
		}

		/**
		 * Apaga contatos dentro do arraylist (nome e chave pix)
		 *
		 *
		 * método recebe como parametro objeto contato e um arraylist
		 * metódo retorna um booelan
		 *
		 * @param contato
		 * @param contatos
		 * @return booelan
		 */
		public boolean apagarContato(int id){
				contatos.remove(id);
			 return true;
		}

		/**
		 *  Verifica  se o valor já foi adicionado ao ArrayList
		 *
		 *
		 * recebe como parametro um objeto contato e um arraylist
		 * retorna um boolean
		 *
		 * @param contato
		 * @return boolean
		 */
		public boolean verificaPorNome(Contato contato) {
			for(int i= 0; i<contatos.size();i++){
				Contato nomeLista = contatos.get(i);
				if(nomeLista.getNome() != contato.getNome()) {
					return true;
				}
			}
			return false;
		}

		/**
		 * Verifica  se o valor já foi adicionado ao ArrayList
		 *
		 *
		 * recebe como parametro um objeto contato e um arraylist
		 * retorna um boolean
		 *
		 * @param contato
		 * @return boolean
		 */
		public  boolean verificaPorChavePix(Contato contato) {
			for(int i=0; i<contatos.size();i++){
				Contato chaveLista = contatos.get(i);
				if(chaveLista.getChavePix() != contato.getChavePix()){
					return true;
				}
			}
			return false;
		}
}
