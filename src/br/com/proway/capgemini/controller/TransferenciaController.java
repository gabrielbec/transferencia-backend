package br.com.proway.capgemini.controller;

import java.time.LocalDate;
import java.time.Period;

import br.com.proway.capgemini.model.Transferencia;

public class TransferenciaController {
	
	Transferencia transferencia;	
	
	/**
	 * Valida CPF
	 * 
	 * Verifica se o CPF est� vazio, se t�m 11
	 * caracteres e se tem letras
	 * 
	 * @param cpf
	 * @return true se o cpf for v�lido
	 */
	public Boolean validateCPF(String cpf) {

		if (!cpf.matches("[0-9]*") || cpf.length() != 11 || cpf.isBlank()) {
			System.out.println("CPF Inv�lido");
			return false;
		}
		System.out.println("CPF V�lido!");
		return true;
	}
	
	/**
	 * Retorna a diferen�a, em meses, entre a
	 * data atual e uma data escolhida
	 * 
	 * @param data data necessaria para o calculo
	 * @return
	 */
	public int diferencaEntreMeses(LocalDate data) {
		LocalDate dataAtual = LocalDate.now();
		Period comparar = Period.between(data, dataAtual);
		return comparar.getMonths();
	}

	/**
	 * Valida valor
	 * 
	 * Verifica se o valor n�o est� vazio
	 * 
	 * @param valor
	 * @return true se o valor n�o for vazio,
	 *         logo � v�lido
	 */
	public Boolean validateValor(String valor) {

		if (!valor.matches("[0-9]*") || valor.isBlank()) {
			return false;
		}
		return true;

	}

}
