package br.com.proway.capgemini.controller;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.math.BigDecimal;

import br.com.proway.capgemini.model.TransferenciaDOCTED;

public class TransferenciaDOCTEDController {

	public TransferenciaDOCTED transferenciaDOCTED;

	/**
	 * Efetua transfer�ncia de valor via DOC ou TED
	 * @param valorTransferencia
	 */
	public void transferir() {
		LocalDateTime currentDateAndTime = LocalDateTime.now();	
		
		if (validaDataDOCTED(currentDateAndTime)) {			
			switch (this.transferenciaDOCTED.getTipo_transferencia()) {			
			case "TED":
				System.out.println("Sucesso!");
				//m�todo POST para armazenar transfer�ncia no banco
				//m�todo GET para exibir o comprovante
				break;				
			case "DOC":
				BigDecimal limite_valor_DOC = new BigDecimal("5000");
				if ((limite_valor_DOC.compareTo(this.transferenciaDOCTED.getValor_transferencia())) == 1) {
					System.out.println("Sucesso!");
					//m�todo POST para armazenar transfer�ncia no banco
					//m�todo GET para exibir o comprovante
					break;
				} else {
					System.out.println("Valor acima do limite permitido para DOC (R$4.999,99)."); //exce��o personalizada
					break;
				}
			}
		} else {
			System.out.println("N�o foi poss�vel efetuar a transfer�ncia."); //Agendar data da transfer�ncia
		}
	}

	/**
	 * Verifica se na data e hor�rio atual ser� poss�vel efetuar um pagamento via DOC ou TED
	 * @param LocalDateTime currentDateAndTime
	 * @return true (caso seja poss�vel)
	 */
	public boolean validaDataDOCTED(LocalDateTime currentDateAndTime) {
		if (currentDateAndTime.getDayOfWeek() != DayOfWeek.SATURDAY && currentDateAndTime.getDayOfWeek() != DayOfWeek.SUNDAY
				&& currentDateAndTime.getHour() >= 6 && currentDateAndTime.getHour() <= 17) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Valida nome do banco
	 * 
	 * Tem a fun��o de validar o banco se o campo estiver em branco
	 * 
	 * @param banco
	 * @return true se o banco for v�lido
	 * 
	 */
	public static Boolean validateBanco(String banco) {
		if (!banco.matches("[A-Za-z]*") || banco.isBlank()) {
			return false;
		}
		return true;
	}

	/**
	 * Valida ag�ncia
	 * 
	 * Faz a valida��o da ag�ncia que deve conter 4 d�gitos, n�o pode ser vazia e
	 * n�o deve conter letras
	 * 
	 * @param agencia
	 */
	public static Boolean validateAgencia(String agencia) {
		if (!agencia.matches("[0-9]*") || agencia.isBlank() || agencia.length() != 4) {
			return false;
		}
		return true;
	}

	/**
	 * Valida conta
	 * 
	 * Valida se a conta tem 6 d�gitos num�ricos e se a conta est� vazia
	 * 
	 * @param conta
	 * @return true se a conta obedecer os crit�rios acima
	 */
	public static Boolean validateConta(String conta) {
		if (!conta.matches("[0-9]*") || conta.length() != 6 || conta.isBlank()) {
			return false;
		}
		return true;
	}

	/**
	 * Valida d�gito
	 * 
	 * Valida se o d�gito cont�m apenas um n�mero e se cont�m letras
	 * 
	 * @param digito
	 * @return true se o d�gito for v�lido
	 */
	public static Boolean validateDigito(String digito) {
		if (!digito.matches("[0-9]*") || digito.isBlank() || digito.length() != 1) {
			return false;
		}
		return true;
	}

	/**
	 * Valida tipo de transfer�ncia (DOC ou TED) *
	 * 
	 * @param tipoTransferencia
	 * @return true (tipo v�lido)
	 */
	public Boolean validateTipoDeTransferencia(String tipoTransferencia) {
		if (tipoTransferencia == "DOC" || tipoTransferencia == "TED") {
			return true;
		} else {
			return false;
		}
	}

}
