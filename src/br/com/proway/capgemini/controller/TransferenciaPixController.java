package br.com.proway.capgemini.controller;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import br.com.proway.capgemini.model.TransferenciaPix;

public class TransferenciaPixController {
	
	TransferenciaPix transferenciaPix;
	
	/**
	 * Valida chave pix
	 * 
	 * Verifica se a chave pix est� vazia e se o
	 * tamanho for maior que 32
	 * 
	 * @param chavepix
	 * @return true se a chave pix cont�m alguma
	 *         string
	 */
	public Boolean validateChavePix(String chavepix) {

		if (chavepix.isBlank() || chavepix.length() > 32) {
			return false;
		}
		return true;
	}	

	/**
	 * Verifica se o celular � valido
	 * 
	 * @param celular
	 * @return
	 */
	public boolean celular(String celular) {
		return celular.matches("(\\(?\\d{2}\\)?\\s)?(\\d{4,5}\\-\\d{4})");
	}

	/**
	 * Verifica se o email � v�lido
	 * 
	 * @param email
	 * @return
	 */
	public boolean email(String email) {
		return email.matches(
			"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$");

	}

	/**
	 * Verica se a chave aleatoria � valida
	 * 
	 * @param celular
	 * @return
	 */
	public boolean chavePixAleatoria(String chavePixAleatoria) {
		return chavePixAleatoria.matches("\\w{8}\\-?\\w{4}\\-?\\w{4}-?\\w{4}\\-?\\w{12}");
	}

	/**
	 * Valida chave pix
	 * 
	 * Verifica se a chave pix est� vazia e se �
	 * email, celular ou chave aleatoria
	 * 
	 * 
	 * @param chavepix
	 * @return true se a chave pix conter os
	 *         valores certos
	 */
	public Boolean validateChavePix2(String chavepix) {

		boolean celular = celular(chavepix);
		boolean email = email(chavepix);
		boolean chavePixAleatoria = chavePixAleatoria(chavepix);

		if (celular || email || chavePixAleatoria) {
			return true;
		}
		return false;

	}

}
