package br.com.proway.capgemini.model;

import java.util.ArrayList;
import java.util.Date;

public class Contato{

	private  int id;
	private String nome;
	private String chavePix;


	public Contato () {
	}

	/**
	 * Construtor
	 * 
	 * 
	 * @param id
	 * @param nome
	 * @param chavePix
	 */
	public Contato(int id, String nome, String chavePix) {
		super();
		this.id = id;
		this.nome = nome;
		this.chavePix = chavePix;
	}

	/**
	 *  retorna id
	 *
	 * @return id
	 */
	public int getId(){
		return id;
	}

	/**
	 *  Atribui id
	 *
	 * @param id
	 */
	public int setId (int id){
		return this.id = id;
	}

	/**
	 *  Retorna o nome
	 *
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Atribui valor ao nome
	 * 
	 * @param nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/**
	 *  retorna valor da chave PIX
	 * 
	 * @return chave pix
	 */
	public String getChavePix() {
		return chavePix;
	}
	
	/**
	 * Atribui valor a chave PIX
	 * 
	 * @param chavePix
	 */
	public void setChavePix(String chavePix) {
		this.chavePix = chavePix;
	}
}
