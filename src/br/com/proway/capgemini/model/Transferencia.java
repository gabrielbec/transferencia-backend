package br.com.proway.capgemini.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public abstract class Transferencia {
	
	private int id;
	private String nome_pagador;
	private String cpfoucnpj_pagador;
	private String nome_recebedor;
	private String cpfoucnpj_recebedor;
	private LocalDateTime data_transferencia;
	private BigDecimal valor_transferencia;
		
	public Transferencia() {
	}

	public Transferencia(int id, String nome_pagador, String cpfoucnpj_pagador, String nome_recebedor,
			String cpfoucnpj_recebedor, LocalDateTime data_transferencia, BigDecimal valor_transferencia) {
		this.id = id;
		this.nome_pagador = nome_pagador;
		this.cpfoucnpj_pagador = cpfoucnpj_pagador;
		this.nome_recebedor = nome_recebedor;
		this.cpfoucnpj_recebedor = cpfoucnpj_recebedor;
		this.data_transferencia = data_transferencia;
		this.valor_transferencia = valor_transferencia;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome_pagador() {
		return nome_pagador;
	}

	public void setNome_pagador(String nome_pagador) {
		this.nome_pagador = nome_pagador;
	}

	public String getCpfoucnpj_pagador() {
		return cpfoucnpj_pagador;
	}

	public void setCpfoucnpj_pagador(String cpfoucnpj_pagador) {
		this.cpfoucnpj_pagador = cpfoucnpj_pagador;
	}

	public String getNome_recebedor() {
		return nome_recebedor;
	}

	public void setNome_recebedor(String nome_recebedor) {
		this.nome_recebedor = nome_recebedor;
	}

	public String getCpfoucnpj_recebedor() {
		return cpfoucnpj_recebedor;
	}

	public void setCpfoucnpj_recebedor(String cpfoucnpj_recebedor) {
		this.cpfoucnpj_recebedor = cpfoucnpj_recebedor;
	}

	public LocalDateTime getData_transferencia() {
		return data_transferencia;
	}

	public void setData_transferencia(LocalDateTime data_transferencia) {
		this.data_transferencia = data_transferencia;
	}

	public BigDecimal getValor_transferencia() {
		return valor_transferencia;
	}

	public void setValor_transferencia(BigDecimal valor_transferencia) {
		this.valor_transferencia = valor_transferencia;
	}

	
	
}
