package br.com.proway.capgemini.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class TransferenciaDOCTED extends Transferencia {
	
	private String banco;
	private String agencia;
	private String conta;
	private String digito_conta;
	private String tipo_transferencia; //DOC ou TED
	
	public TransferenciaDOCTED() {
	}
	
	public TransferenciaDOCTED(int id, String nomePagador, String cpfOuCnpjPagador, String nomeRecebedor,
			String cpfOuCnpjRecebedor, LocalDateTime dataTransferencia, BigDecimal valorTransferencia, String banco, String agencia,
			String conta, String digito_conta, String tipo_transferencia) {
		super(id, nomePagador, cpfOuCnpjPagador, nomeRecebedor, cpfOuCnpjRecebedor, dataTransferencia,
				valorTransferencia);
		this.banco = banco;
		this.agencia = agencia;
		this.conta = conta;
		this.digito_conta = digito_conta;
		this.tipo_transferencia = tipo_transferencia;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getConta() {
		return conta;
	}

	public void setConta(String conta) {
		this.conta = conta;
	}

	public String getDigito_conta() {
		return digito_conta;
	}

	public void setDigito_conta(String digito_conta) {
		this.digito_conta = digito_conta;
	}

	public String getTipo_transferencia() {
		return tipo_transferencia;
	}

	public void setTipo_transferencia(String tipo_transferencia) {
		this.tipo_transferencia = tipo_transferencia;
	}

}
