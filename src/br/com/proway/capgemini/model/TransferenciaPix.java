package br.com.proway.capgemini.model;

import java.util.Date;

public class TransferenciaPix extends Transferencia {
	
	private String chavePix;
	private String tipoChave;
	
	public TransferenciaPix(int id, String nomePagador, String cpfOuCnpjPagador, String nomeRecebedor,
			String cpfOuCnpjRecebedor, Date dataTransferencia, double valorTransferencia, String chavePix,
			String tipoChave) {
		super(id, nomePagador, cpfOuCnpjPagador, nomeRecebedor, cpfOuCnpjRecebedor, dataTransferencia,
				valorTransferencia);
		this.chavePix = chavePix;
		this.tipoChave = tipoChave;
	}

	public String getChavePix() {
		return chavePix;
	}

	public void setChavePix(String chavePix) {
		this.chavePix = chavePix;
	}

	public String getTipoChave() {
		return tipoChave;
	}

	public void setTipoChave(String tipoChave) {
		this.tipoChave = tipoChave;
	}	

}
