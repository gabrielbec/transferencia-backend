import java.util.ArrayList;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import br.com.proway.capgemini.controller.ContatoController;
import br.com.proway.capgemini.model.Contato;


class ContatoTeste {


	@Test
	void ContatoPixGettersESetters() {
		Contato contato = new Contato();
		ContatoController contatoController = new ContatoController();
		contatoController.contato = contato;
		contatoController.contato.setId(0);
		contatoController.contato.setNome("Eduarda");
		contatoController.contato.setChavePix("carmoduda6@gmail.com");
		Assert.assertEquals(0, contatoController.contato.getId());
		Assert.assertEquals("Eduarda", contatoController.contato.getNome());
		Assert.assertEquals("carmoduda6@gmail.com", contatoController.contato.getChavePix());
	}

	@Test
	 void salvarContatoParametroContatoRetornaContato(){
		Contato contato = new Contato(0,"nome","1111-1111");
		ContatoController contatoController = new ContatoController();
		contatoController.salvarContato(contato);
		Assert.assertEquals(contato.getNome(), contatoController.contatos.get(0).getNome());
		Assert.assertEquals(contato.getChavePix(), contatoController.contatos.get(0).getChavePix());				
	}
	

	@Test
	void listarContatoRetornaContato(){
		Contato contato = new Contato(0,"nome","1111-1111");
		ContatoController contatoController = new ContatoController();
		contatoController.salvarContato(contato);
		Assert.assertEquals(contatoController.contatos, contatoController.listarContato());
	}

	@Test
	void atualizarContatoParametroContatoIdRetornaContato(){
		Contato contato = new Contato(0,"nome","1111-1111");
		ContatoController contatoController = new ContatoController();
		contatoController.salvarContato(contato);
		Assert.assertEquals(contatoController.contatos, contatoController.atualizarContato(contato, 0));
	}

	@Test
	void apagarContatoParametroIdRetornaTrue() {
		Contato contato = new Contato(0,"nome","1111-1111");
		ContatoController contatoController = new ContatoController();
		contatoController.salvarContato(contato);
		Assert.assertTrue(contatoController.apagarContato(0));
	}

	@Test
	void verificaPorNomeContatoRetornaTrue() {
		Contato contato1 = new Contato(0,"nome","1111-1111");
		Contato contato2 = new Contato(1,"sobrenome","2222-2222");
		ContatoController contatoController = new ContatoController();
		contatoController.salvarContato(contato1);
		Assert.assertTrue(contatoController.verificaPorNome(contato2));
	}
	
	@Test
	void verificaPorNomeContatoRetornaFalse() {
		Contato contato = new Contato(0,"nome","1111-1111");
		ContatoController contatoController = new ContatoController();
		contatoController.salvarContato(contato);
		Assert.assertFalse(contatoController.verificaPorNome(contato));
	}

	@Test
	void  verificaPorChavePixContatoRetornaTrue() {
		Contato contato1 = new Contato(0,"nome","1111-1111");
		Contato contato2 = new Contato(1,"sobrenome","2222-2222");
		ContatoController contatoController = new ContatoController();
		contatoController.salvarContato(contato1);
		Assert.assertTrue(contatoController.verificaPorChavePix(contato2));
	}

	@Test
	void  verificaPorChavePixContatoRetornaFalse() {
		Contato contato = new Contato(0,"nome","1111-1111");
		ContatoController contatoController = new ContatoController();
		contatoController.salvarContato(contato);
		Assert.assertFalse(contatoController.verificaPorChavePix(contato));
	}
}
