package br.com.proway.capgemini.testes;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import br.com.proway.capgemini.controller.TransferenciaController;

class TransferenciaControllerTest {
	
	//Validações de CPF
	
	@Test
	void validateCPFRetornaTrue() {
		TransferenciaController transferenciaController = new TransferenciaController();
		Assert.assertTrue(transferenciaController.validateCPF("02775321321"));
	}

	@Test
	void validateCPFRetornaFalseIsBlank() {
		TransferenciaController transferenciaController = new TransferenciaController();
		Assert.assertFalse(transferenciaController.validateCPF(""));
	}

	@Test
	void validateCPFRetornaFalseLetras() {
		TransferenciaController transferenciaController = new TransferenciaController();
		Assert.assertFalse(transferenciaController.validateCPF("abcdefghijk"));
	}

	@Test
	void validateCPFRetornaFalseMaiorQue11Digitos() {
		TransferenciaController transferenciaController = new TransferenciaController();
		Assert.assertFalse(transferenciaController.validateCPF("123456789101"));
	}

	@Test
	void validateCPFRetornaFalseMenorQue11Digitos() {
		TransferenciaController transferenciaController = new TransferenciaController();
		Assert.assertFalse(transferenciaController.validateCPF("1"));
	}

	@Test
	void validateCPFRetornaFalseLetrasENumeros() {
		TransferenciaController transferenciaController = new TransferenciaController();
		Assert.assertFalse(transferenciaController.validateCPF("1234567891a"));
	}
	
	//Validações de Data
	
	@Test
	void diferencaEntreMesesValorVerdadeiro() {
		TransferenciaController transferenciaController = new TransferenciaController();
		LocalDate dataEscolhida = LocalDate.of(2021, 6, 1);
		Assert.assertEquals(transferenciaController.diferencaEntreMeses(dataEscolhida), 2);
	}

	@Test
	void diferencaEntreMesesValorFalso() {
		TransferenciaController transferenciaController = new TransferenciaController();
		LocalDate dataEscolhida = LocalDate.of(2021, 6, 1);
		Assert.assertNotEquals(transferenciaController.diferencaEntreMeses(dataEscolhida), 1);
	}
	
	//Validações de Valor
	
	@Test
	void validateValorRetornaTrue() {
		TransferenciaController transferenciaController = new TransferenciaController();
		Assert.assertTrue(transferenciaController.validateValor("1233"));
	}
	
	@Test
	void validateValorRetornaFalseIsBlank() {
		TransferenciaController transferenciaController = new TransferenciaController();
		Assert.assertFalse(transferenciaController.validateValor(""));
	}
	
	@Test
	void validateValorRetornaFalseLetras() {
		TransferenciaController transferenciaController = new TransferenciaController();
		Assert.assertFalse(transferenciaController.validateValor("aaaa"));
	}
	
	@Test
	void validateValorRetornaFalseLetrasENumeros() {
		TransferenciaController transferenciaController = new TransferenciaController();
		Assert.assertFalse(transferenciaController.validateValor("1b23a"));
	}

}
