package br.com.proway.capgemini.testes;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import br.com.proway.capgemini.controller.TransferenciaDOCTEDController;
import br.com.proway.capgemini.model.TransferenciaDOCTED;

class TransferenciaDOCTEDControllerTest {
	
	//Valida��o do m�todo validaDataDOCTED()
	
	@Test
	void validaDataDOCTEDRetornaTrueSeDataAtualForDiaUtilEHorarioComercial() {
		TransferenciaDOCTED transferenciaDOCTED = new TransferenciaDOCTED();
		TransferenciaDOCTEDController transferenciaDOCTEDController = new TransferenciaDOCTEDController();
		transferenciaDOCTEDController.transferenciaDOCTED = transferenciaDOCTED;
		boolean condition = transferenciaDOCTEDController.validaDataDOCTED(LocalDateTime.of(2021, 8, 27, 06, 30)); //sexta-feira, em hor�rio permitido
		Assert.assertTrue(condition);
	}
		
	@Test
	void validaDataDOCTEDRetornaFalseSeDataAtualForFinalDeSemana() {
		TransferenciaDOCTED transferenciaDOCTED = new TransferenciaDOCTED();
		TransferenciaDOCTEDController transferenciaDOCTEDController = new TransferenciaDOCTEDController();
		transferenciaDOCTEDController.transferenciaDOCTED = transferenciaDOCTED;
		boolean condition = transferenciaDOCTEDController.validaDataDOCTED(LocalDateTime.of(2021, 8, 28, 17, 17)); //s�bado, em hor�rio permitido
		Assert.assertFalse(condition);
	}
	
	@Test
	void validaDataDOCTEDRetornaFalseSeEstiverForaDoHorarioComercial() {
		TransferenciaDOCTED transferenciaDOCTED = new TransferenciaDOCTED();
		TransferenciaDOCTEDController transferenciaDOCTEDController = new TransferenciaDOCTEDController();
		transferenciaDOCTEDController.transferenciaDOCTED = transferenciaDOCTED;
		boolean condition = transferenciaDOCTEDController.validaDataDOCTED(LocalDateTime.of(2021, 8, 27, 20, 0)); //sexta-feira, por�m fora do hor�rio permitido
		Assert.assertFalse(condition);
	}
	
	//Valida��o do m�todo investir()
	
	@Test
	void transferirDeveEntrarNoLacoDaTransferenciaPorTED() {
		TransferenciaDOCTED transferenciaDOCTED = new TransferenciaDOCTED(1, "Fulano de Tal", "15465498765", "Ciclano de Tal", "13464685555", null, null, "256", "1011", "1555648", "1", "TED");
		TransferenciaDOCTEDController transferenciaDOCTEDController = new TransferenciaDOCTEDController();
		transferenciaDOCTEDController.transferenciaDOCTED = transferenciaDOCTED;
		transferenciaDOCTEDController.transferir();
		Assert.assertEquals("TED", transferenciaDOCTEDController.transferenciaDOCTED.getTipo_transferencia());
	}
	
	@Test
	void transferirDeveEntrarNoLacoDaTransferenciaPorDOC() {
		TransferenciaDOCTED transferenciaDOCTED = new TransferenciaDOCTED(1, "Fulano de Tal", "15465498765", "Ciclano de Tal", "13464685555", LocalDateTime.of(2021, 8, 26, 10, 30), BigDecimal.valueOf(4000), "256", "1011", "1555648", "1", "DOC");
		TransferenciaDOCTEDController transferenciaDOCTEDController = new TransferenciaDOCTEDController();
		transferenciaDOCTEDController.transferenciaDOCTED = transferenciaDOCTED;
		transferenciaDOCTEDController.transferir();
		Assert.assertEquals("DOC", transferenciaDOCTEDController.transferenciaDOCTED.getTipo_transferencia());
	}
	
	@Test
	void transferirDeveEntrarNoLacoDaTransferenciaPorDOCMasEntrarNoElseDevidoAoLimiteDeValor() {
		TransferenciaDOCTED transferenciaDOCTED = new TransferenciaDOCTED(1, "Fulano de Tal", "15465498765", "Ciclano de Tal", "13464685555", LocalDateTime.of(2021, 8, 26, 10, 30), BigDecimal.valueOf(5000), "256", "1011", "1555648", "1", "DOC");
		TransferenciaDOCTEDController transferenciaDOCTEDController = new TransferenciaDOCTEDController();
		transferenciaDOCTEDController.transferenciaDOCTED = transferenciaDOCTED;
		transferenciaDOCTEDController.transferir();
		Assert.assertEquals(BigDecimal.valueOf(5000), transferenciaDOCTEDController.transferenciaDOCTED.getValor_transferencia());
	}
	
	@Test
	void transferirDeveVerificarQueADataEstaInvalidaEIrParaOElse() {
		TransferenciaDOCTED transferenciaDOCTED = new TransferenciaDOCTED(1, "Fulano de Tal", "15465498765", "Ciclano de Tal", "13464685555", LocalDateTime.of(2021, 8, 28, 10, 30), BigDecimal.valueOf(5000), "256", "1011", "1555648", "1", "DOC");
		TransferenciaDOCTEDController transferenciaDOCTEDController = new TransferenciaDOCTEDController();
		transferenciaDOCTEDController.transferenciaDOCTED = transferenciaDOCTED;
		transferenciaDOCTEDController.transferir();
		Assert.assertEquals(LocalDateTime.of(2021, 8, 28, 10, 30), transferenciaDOCTEDController.transferenciaDOCTED.getData_transferencia());
	}

	//Valida��o do tipo de transfer�ncia
	
//	@Test
//	void validateTipoDeTransferenciaRetornaTrue() {
//		TransferenciaDOCTEDController transferenciaDOCTEDController = new TransferenciaDOCTEDController();
//		Assert.assertTrue(transferenciaDOCTEDController.validateTipoDeTransferencia("pix"));
//	}
//	
//	@Test
//	void validateTipoDeTransferenciaRetornaFalseIsBlank() {
//		TransferenciaDOCTEDController transferenciaDOCTEDController = new TransferenciaDOCTEDController();
//		Assert.assertFalse(transferenciaDOCTEDController.validateTipoDeTransferencia(""));
//	}
//	
//	@Test
//	void validateTipoDeTransferenciaRetornaFalseNumero() {
//		TransferenciaDOCTEDController transferenciaDOCTEDController = new TransferenciaDOCTEDController();
//		Assert.assertFalse(transferenciaDOCTEDController.validateTipoDeTransferencia("12"));
//	}

}
