package br.com.proway.capgemini.testes;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import br.com.proway.capgemini.controller.TransferenciaPixController;

class TransferenciaPixControllerTest {

	@Test
	void validateChavePixRetornaTrue() {
		TransferenciaPixController transferenciaPixController = new TransferenciaPixController();
		Assert.assertTrue(transferenciaPixController.validateChavePix("12345678123456781234567812345678"));
	}
	
	@Test
	void validateChavePixRetornaFalseIsBlank() {
		TransferenciaPixController transferenciaPixController = new TransferenciaPixController();
		Assert.assertFalse(transferenciaPixController.validateChavePix(""));
	}
	
	@Test
	void validateChavePixRetornaFalseMaiorQue32Digitos() {
		TransferenciaPixController transferenciaPixController = new TransferenciaPixController();
		Assert.assertFalse(transferenciaPixController.validateChavePix("123456781234567812345678123456789"));
	}
	
	@Test
	void celularValido() {
		TransferenciaPixController transferenciaPixController = new TransferenciaPixController();
		Assert.assertTrue(transferenciaPixController.celular("99999-9999"));
	}
	
	@Test
	void celularInvalido() {
		TransferenciaPixController transferenciaPixController = new TransferenciaPixController();
		Assert.assertFalse(transferenciaPixController.celular("999999999"));
	}
	
	@Test
	void celularValidoDDD() {
		TransferenciaPixController transferenciaPixController = new TransferenciaPixController();
		Assert.assertTrue(transferenciaPixController.celular("(71) 99999-9999"));
	}
	
	@Test
	void celularInvalidoDDD() {
		TransferenciaPixController transferenciaPixController = new TransferenciaPixController();
		Assert.assertFalse(transferenciaPixController.celular("(71)99999-9999"));
	}
	
	@Test
	void emailValido() {
		TransferenciaPixController transferenciaPixController = new TransferenciaPixController();
		Assert.assertTrue(transferenciaPixController.email("gabriel@gmail.com"));
	}
	
	@Test
	void emailInvalido() {
		TransferenciaPixController transferenciaPixController = new TransferenciaPixController();
		Assert.assertFalse(transferenciaPixController.email("gabriel@@teste.com.br.ajsbdk"));
	}
	
	@Test
	void chaveAleatoriaTrue() {
		TransferenciaPixController transferenciaPixController = new TransferenciaPixController();
		Assert.assertTrue(transferenciaPixController.chavePixAleatoria("dbbf965d-677c-49ff-b9da-5131da150454"));
	}
	
	@Test
	void chaveAleatoriaFalse() {
		TransferenciaPixController transferenciaPixController = new TransferenciaPixController();
		Assert.assertFalse(transferenciaPixController.chavePixAleatoria("dbb-556677c-49fa4af-da-51-50-454"));
	}
	
	@Test
	void validaChavePixAleatoria() {
		TransferenciaPixController transferenciaPixController = new TransferenciaPixController();
		Assert.assertTrue(transferenciaPixController.validateChavePix2("db5h965d-87w9-8w6s-b9da-5131da150454"));
	}
	
	@Test
	void validaChavePixTelefone() {
		TransferenciaPixController transferenciaPixController = new TransferenciaPixController();
		Assert.assertTrue(transferenciaPixController.validateChavePix2("(78) 99712-3456"));
	}
	
	@Test
	void validaChavePixEmail() {
		TransferenciaPixController transferenciaPixController = new TransferenciaPixController();
		Assert.assertTrue(transferenciaPixController.validateChavePix2("gabriel_hacker@soumesmo.com.br"));
	}
	
	@Test
	void validaChavePixFalse() {
		TransferenciaPixController transferenciaPixController = new TransferenciaPixController();
		Assert.assertFalse(transferenciaPixController.validateChavePix2("db5h9d-879-8-6s-b9da-5131d50454"));
	}
}
